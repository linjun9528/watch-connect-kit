# watch-connect-kit openharmony demo 开发流程

demo主要是介绍手机上三方应用如何集成和使用社区提供的 watch-connect-kit ，连接 watch-connect-app 与 watch 上的应用通讯。

## 1. demo开发环境

DevEco Studio 4.1 Release

OpenHarmony 4.1.7.5 Release

## 2. 集成Kit准备

### 2.1 Kit下载
watch-connect-kit 静态共享包 [点我下载](https://gitee.com/cooperation-team-L0UI/watch-connect-kit/blob/master/kit/openharmony/watchConnectKit.har)。


### 2.2 工程集成Kit

2.2.1.将下载的kit静态共享包放到项目目录中。

2.2.2.在项目oh-package.json5中引入kit依赖包。目录结构如下：

```shell
"dependencies": {
  'watchConnectKit': "file:../kit/watchConnectKit.har"
}
```
### 2.3 kit接口定义

社区 watch-connect-kit 提供了接口定义及使用到的结构体等信息，可先查看熟悉。

kit接口定义链接：[点我查看](https://gitee.com/cooperation-team-L0UI/doc/blob/master/%E6%89%8B%E6%9C%BA%E6%89%8B%E8%A1%A8%E4%BA%92%E8%81%94%E5%BC%80%E5%8F%91/phone/watch-connect-kit-openHarmony.md)。

### 2.4 Demo

demo链接：[点我查看](https://gitee.com/cooperation-team-L0UI/watch-connect-kit/tree/master/demo/phone/openharmony)。

## 3. 集成Kit准备

### 3.1 权限配置
watch-connect-kit-demo 会使用到蓝牙连接，获取应用列表功能，需要添加对应权限。
```shell
"requestPermissions": [
      {
        "name": "ohos.permission.ACCESS_BLUETOOTH"
      },
      {
        "name": "ohos.permission.GET_INSTALLED_BUNDLE_LIST"
      }
    ]
```


### 3.2 Kit初始化
导入模块
```typescript
import { WatchConnectEngine } from 'watchConnectKit';
```

### 3.3 调用Kit接口方法

3.3.1.watch-connect-kit主要实现了getBondedDevices,getAvailableKbytes,ping,send,cancelFileTransfer,isAppInstalled,getAppVersion,registerReceiver ,unregisterReceiver等接口。

3.3.2.以send方法为例，send方法作用是向需要通信的穿戴设备侧应用发送通信消息。可发送文本或是文件消息。
#### 示例

```typescript
import { Device, Message, Peer, SendCallback, WatchConnectEngine, CAPABILITY, ErrCode} from 'watchConnectKit';
let selectedDevice = new Device('', 'name', 'uuid', 0, '', '', false, CAPABILITY.DEVICE_CAPABILITY_SUPPORT, CAPABILITY.DEVICE_CAPABILITY_SUPPORT, CAPABILITY.DEVICE_CAPABILITY_SUPPORT, 0);
let peer = new Peer(selectedDevice, 'fingerPrint', 'srcPkgName', 'dstPkgName');
let message: Message = new Message(0, 'hello', '', '', '');
let sendCallBack: SendCallback = {
  onSendProgress: (progress: number) => {
    console.log(TAG, 'onSendProgress, progress:' + progress);
  },
  onSendResult: (code: ErrCode) => {
    console.log(TAG, 'onSendResult, code:' + code);
  }
}
let watchConnectEngine: WatchConnectEngine = new WatchConnectEngine();
watchConnectEngine.send(peer, message, sendCallBack);
```

## 4. 注意事项

### 4.1 权限问题
开发过程中可能会使用到蓝牙连接，获取应用列表，文件传输等功能，需要添加对应权限。

### 4.2 首次操作问题
应用首次调用接口方法时需要进行BLE蓝牙连接，可能耗时较长。

### 4.3 操作顺序问题
应用和表侧建立连接需要先通过getBondedDevices获取到设备列表，选择设备之后可以执行发送消息文件等方法。

