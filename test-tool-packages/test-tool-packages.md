# 测试包说明

目前支持Android，iOS，openharmony

Android 目前有4个demo测试安装包，如下：

+ watchConnectKitDemo.apk：集成kit的demo，模拟三方应用。
+ watchConnectApp.apk：集成kit，提供服务，模拟表端返回数据。
+ wrtm.apk：集成kit，提供服务，会连接表端，返回表端数据。目前只支持海思开发板。
+ androidTool.apk: 支持蓝牙连接手表，发送与下载文件，安装与卸载应用。目前只支持海思开发板。

iOS 目前有2个demo测试安装包，如下：

+ WatchAppDemo.ipa：集成kit的demo，模拟三方应用。
+ watchConnectMarket.ipa：集成kit，提供服务，模拟应用市场的三方应用。

openharmony 目前有2个demo测试安装包，如下：

+ watchConnectDemo.hap：集成kit的demo，模拟三方应用。
+ watchConnectMarket.hap：集成kit，提供服务，模拟应用市场的三方应用。

watchApplication 目前有3个测试安装包，如下：

+ alarmClock.bin：模拟表侧应用。
+ musicPlayer.bin：模拟表侧应用。
+ stopwatch.bin：模拟表侧应用。

## 测试方法

### 三方应用开发

如果是做三方应用开发，可以使用 **watchConnectApp** 和 **wrtm** 来测试，响应三方应用调用。在前期表端对应的应用没有开发完成前，可以先用 **watchConnectApp** 来模拟表端返回数据，在后期表端对应的应用开发完之后，可以使用 **wrtm** 连接表端，返回正式数据。
也可以使用**androidTool**，通过蓝牙连接页面搜索表盘进行蓝牙连接，而后有需要的情况下使用文件传输页面给表端传送文件，或者获取表端日志，或者使用应用安装功能进行表盘应用安装并可查看安装日志，也可对安装的应用进行卸载，也便于开发人员开发三方应用。

### 服务端开发

如果是开发服务端，可以使用 **watchConnectKitDemo** 来给服务端喂数据，测试集成kit后的功能。


