import brightness from '@system.brightness'
import app from '@system.app'
import file from '@system.file';
import watchConnect from '@ohos.watchConnect'

export default {
    data: {
        operateMessage: 'Operation info',
        receiveMessgeOK: '',
    },
    onInit() {
        brightness.setKeepScreenOn({
            keepScreenOn: true,
            success: function () {
                console.log('screen on success');
            },
            fail: function () {
                console.log('screen on failed');
            },
        });

        file.writeText({
            uri: 'internal://app/config.json',
            text: 'send message to test',
            success: function() {
                console.log('call writeText success.');
            },
            fail: function(data, code) {
                console.error('write call fail callback fail, code: ' + code + ', data: ' + data);
            },
        });

    },
    onDestroy() {},
    swipeEvent(e) {
        if (e.direction === 'right') {
            app.terminate();
        }
    },
    setPeerInfo() {
        var obj = this;
        console.log('setPeerInfo button click');
        this.operateMessage = "setPeerInfo button click";
        watchConnect.setPeerInfo({
            bundleName: "com.demo.watchconnectkitdemo", // 对端应用bundleName
            fingerPrint: "xxx", // 对端应用的指纹证书
            success: function() {
                console.info('setPeerInfo success');
                obj.receiveMessgeOK = 'setPeerInfo success';
            },
            fail: function(data, code) {
                console.error('setPeerInfo failed. Code: ' + code + '; Data: ' + data);
                obj.receiveMessgeOK = 'setPeerInfo fail ' + data;
            },
        });
    },

    registerMessage() {
        var obj = this;
        console.log('Register message button click');
        obj.operateMessage = 'Register message button click';
        watchConnect.registerReceiver({
            success: function () {
                obj.receiveMessgeOK = 'Message receive success';
            },
            fail: function(data, code) {
                console.error('unregisterReceiver failed. Code: ' + code + '; Data: ' + data);
                obj.receiveMessgeOK = 'Message receive fail. Code: ' + code + '; Data: ' + data;
            },
            onReceiveMessage: function (data) {
                obj.receiveMessgeOK = 'Receive message info:' + data;

                console.info("registerMessage onReceiveMessage : " + JSON.stringify(data));
            },
            progress: function(count) {
                console.info('registerReceiver recive progress: ' + count);
            },
        });
    },

    unregisterMessage() {
        this.operateMessage = "unregisterMessage message button click";
        var flash = this;
        watchConnect.unregisterReceiver({
            success: function () {
                flash.operateMessage = "Stop receiving messages is sent";
            },
            fail: function(data, code) {
                console.error('unregisterReceiver failed. Code: ' + code + '; Data: ' + data);
            },
        });
    },

    sendMessage() {
        this.operateMessage = "sendMessage button click";
        let obj = this;
        let dataInfo = {
            type: 'message',
            name: 'hello watchConnect',
            content: 'hello watchConnect is demo',
        };
        watchConnect.send({
            message: dataInfo,
            success: function () {
                obj.operateMessage = 'Message send successfully';
            },
            fail: function (data, code) {
                obj.operateMessage = 'Failed to send message. Code: ' + code + '; Data: ' + data;
            },
            onSendResult: function (data, code) {
                console.info("sendMessage onSendResult : Code: " + code + '; Data: ' + data);
            },
            onSendProgress: function (count) {
                console.info(count);
            },
        });
    },

    sendFile() {
        this.operateMessage = 'sendFile button click';
        let dataInfo = {
            type: 'file',
            name: 'config.json',
            content: 'internal://app/config.json',
            filemode: 'text',
            permission: 'R'
        };

        let obj = this;
        watchConnect.send({
            message: dataInfo,
            success: function () {
                obj.operateMessage = 'file send successfully';
            },
            fail: function () {
                obj.operateMessage = 'Failed to send file';
            },
            onSendResult: function (data, code) {
                console.info("sendFile onSendResult : Code: " + code + '; Data: ' + data);
            },
            onSendProgress: function (count) {
                console.info(count);
            },
        });
    },

    pingRight() {
        console.log('ping right');
        var obj = this;
        obj.operateMessage = 'Ping correct APP';
        watchConnect.ping({
            message: 'com.ximalayaos.app.sport',
            success: function () {
                obj.operateMessage = obj.operateMessage + 'success';
            },
            fail: function () {
                obj.operateMessage = obj.operateMessage + 'fail';
            },
            onPingResult: function (data, code) {
                obj.operateMessage = 'result code:' + code + ', the app already have installed';
                console.info("ping true onSendResult : Code: " + code + '; Data: ' + data);
            },
        });

    },
    pingFalse() {
        console.log('ping false');
        var obj = this;
        obj.operateMessage = 'Ping wrong APP';
        watchConnect.ping({
            message: 'com.ximalayaos.app.sport111',
            onSuccess: function () {
                obj.operateMessage = obj.operateMessage + 'success';
            },
            onFailure: function () {
                obj.operateMessage = obj.operateMessage + 'fail';
            },
            onPingResult: function (data, code) {
                obj.operateMessage = 'result code:' + code + ', the app not installed';
                console.info("ping false onSendResult : Code: " + code + '; Data: ' + data);
            },
        });
    },

    getLocalVersion() {
        console.info("get local click");
        let version = watchConnect.getLocalVersion();
        console.info('getLocalVersion version:' + version);
    },

    getPeerVersion() {
        console.info("get peer click")
        watchConnect.getPeerVersion({
            onGetVersion: function(version) {
                console.info('getPeerVersion version:' + version);
            },
        });
    }
}

