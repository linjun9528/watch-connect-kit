## 使用说明

使用DevEco Studio开发工具打开工程，开发工具[下载地址](https://developer.huawei.com/consumer/cn/download/deveco-studio)

@ohos.watchConnect.d.ts [下载地址](https://e.gitee.com/kunyuan-hongke/repos/cooperation-team-L0UI/doc/blob/master/%E6%89%8B%E6%9C%BA%E6%89%8B%E8%A1%A8%E4%BA%92%E8%81%94%E5%BC%80%E5%8F%91/watch/@ohos.watchConnect.d.ts)

@ohos.watchConnect.d.ts 放置路径，在Deveco Studio 配置的SDK下`12\js\api`

目录

## 约束

DevEco Studio 版本使用 5.0.3.500 以上，SDK 使用API 12版本