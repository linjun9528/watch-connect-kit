package com.demo.watchconnectkitdemo;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.List;

public class SelectServiceActivity extends AppCompatActivity {
    private static final String TAG = "test-kitDemo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_select_service);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        LinearLayout layout = findViewById(R.id.main);
        Log.i(TAG, "SelectServiceActivity in");
        PackageManager packageManager = getPackageManager();
        Intent intent = new Intent();
        intent.setAction("OH_WATCH_CONNECT_ACTION");
        List<ResolveInfo> services = packageManager.queryIntentServices(intent, 0);
        for (ResolveInfo server : services) {
            Log.i(TAG, "SelectServiceActivity serviceInfo.package name:" + server.serviceInfo.packageName);
            Log.i(TAG, "SelectServiceActivity applicationInfo.package name:" + server.serviceInfo.applicationInfo.packageName);
            Button btn = new Button(this);
            btn.setText(server.serviceInfo.packageName);
            btn.setOnClickListener(v -> {
                Log.i(TAG, "SelectServiceActivity serviceInfo.package name:" + server.serviceInfo.packageName);
                Intent it = new Intent();
                it.putExtra("packageName", server.serviceInfo.packageName);
                setResult(RESULT_OK, it);
                finish();
            });

            layout.addView(btn);
        }
    }
}
