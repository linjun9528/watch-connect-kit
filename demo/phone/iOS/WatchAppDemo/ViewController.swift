//
//  ViewController.swift
//  WatchAppDemo
//
//  Created by apple on 2024/11/12.
//

import UIKit
import WatchConnectKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIDocumentPickerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let device = deviceList[indexPath.item]
        let checkBoxBackgroundView = UIView()
        checkBoxBackgroundView.frame = CGRect(origin: CGPoint(x: 20, y: 10), size: CGSize(width: 20, height: 20))
        checkBoxBackgroundView.layer.cornerRadius = 10
        checkBoxBackgroundView.layer.borderColor = selectedDevice?.name == device.name ? UIColor.cyan.cgColor : UIColor.lightGray.cgColor
        checkBoxBackgroundView.layer.borderWidth = 2
        
        let checkBoxView = UIView()
        checkBoxView.frame = CGRect(origin: CGPoint(x: 25, y: 15), size: CGSize(width: 10, height: 10))
        checkBoxView.layer.cornerRadius = 5
        checkBoxView.backgroundColor =  selectedDevice?.name == device.name ? .cyan : UIColor.clear
        cell.addSubview(checkBoxBackgroundView)
        cell.addSubview(checkBoxView)
        
        let nameLabel = UILabel()
        nameLabel.text = deviceList[indexPath.item].name
        nameLabel.frame = CGRect(x: 50, y: 5, width: 300, height: 30)
        cell.addSubview(nameLabel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedDevice = deviceList[indexPath.item]
        tableView.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        appPkgNameTextField?.resignFirstResponder()
        msgContentTextField?.resignFirstResponder()
        dstFilePathTextField?.resignFirstResponder()
        return true
    }
    @objc
    func getBoundDevices() {
        printText(text: "getBoundDevices")
        self.deviceList.removeAll()
        self.watchConnect?.getBondedDevices(getBondedDevicesCallBack: { watchConnectDeviceList in
            self.deviceList = watchConnectDeviceList
            self.tableView?.reloadData()
            self.printText(text: "getBoundDevices success")
        })
    }
    @objc
    func getAvailableKbytes() {
        printText(text: "getAvailableKbytes")
        guard let sDvice = selectedDevice else {
            printText(text: "未选中设备")
            return
        }
        self.watchConnect?.getAvailableKbytes(device: sDvice, getAvailableKbytesCallBack: { freeSize in
            self.printText(text: "getAvailableKbytes freeSize: \(freeSize)Kb")
        })
    }
    @objc
    func pingApp() {
        printText(text: "pingApp")
        let (isSuccess, peer) = self.getPeer()
        if !isSuccess {
            printText(text: "pingApp 未获取到peer")
            return
        }
        self.watchConnect?.ping(peer: peer!, pingCallback: { code in
            self.printText(text: "pingApp result：\(code)")
        })
    }
    @objc
    func isAppInstalled() {
        printText(text: "isAppInstalled")
        let (isSuccess, peer) = self.getPeer()
        if !isSuccess {
            printText(text: "isAppInstalled 未获取到peer")
            return
        }
        self.watchConnect?.isAppInstalled(peer!, installCallBack: { result in
            self.printText(text: "isAppInstalled result：\(result)")
            if result == 208 {
                self.printText(text: "已安装")
            }
        })
    }
    @objc
    func getAppVersion() {
        printText(text: "getAppVersion")
        let (isSuccess, peer) = self.getPeer()
        if !isSuccess {
            printText(text: "getAppVersion 未获取到peer")
            return
        }
        self.watchConnect?.getAppVersion(peer: peer!, versionCallback: { code in
            self.printText(text: "getAppVersion result：\(code)")
        })
    }
    @objc
    func sendMsg() {
        printText(text: "sendMsg")
        let (isSuccess, peer) = self.getPeer()
        if !isSuccess {
            printText(text: "sendMsg 未获取到peer")
            return
        }
        
        guard let payload = msgContentTextField?.text else { return }
        let msg = Message(type: 0, data: Utils.stringToBytes(str: payload), description: "", srcFilePath: "", dstFilePath: "")
        self.watchConnect?.send(peer: peer!, message: msg, onSendProgress: { progress in
            
        }, onSendResult: { code in
            self.printText(text: "sendMsg result：\(code)")
        })
    }
    @objc
    func sendFile() {
        printText(text: "sendFile")
        // 设置可以选择的文件类型，这里选择所有文档类型
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.item"], in:.import)
        documentPicker.delegate = self
        documentPicker.allowsMultipleSelection = false  // 不允许多选
        present(documentPicker, animated: true, completion: nil)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if let selectedFileURL = urls.first {
            let (isSuccess, peer) = self.getPeer()
            if !isSuccess {
                printText(text: "sendFile 未获取到peer")
                return
            }
            let srcfilePath = selectedFileURL.path; // 手机端的文件绝对路径
            let dstFilePath = ""; // 表端的文件路径
            let message = Message(type: 1, data: [],  description: "", srcFilePath: srcfilePath, dstFilePath: dstFilePath);
            self.watchConnect?.send(peer: peer!, message: message, onSendProgress: { progress in
                self.printText(text: "sendFile progress：\(progress)")
            },onSendResult: { resultCode in
                self.printText(text: "sendFile result：\(resultCode)")
            })
        }
    }
    @objc
    func cancelSendFile() {
        printText(text: "cancelSendFile")
        let (isSuccess, peer) = self.getPeer()
        if !isSuccess {
            printText(text: "cancelSendFile 未获取到peer")
            return
        }
        let msg = Message(type: 0, data: [], description: "", srcFilePath: "", dstFilePath: "")
        self.watchConnect?.cancelFileTransfer(peer: peer!, message: msg , cancelFileTransferCallBack: { resultCode in
            self.printText(text: "cancelSendFile result：\(resultCode)")
        })
    }
    @objc
    func receiveMsg() {
        printText(text: "receiveMsg")
        let (isSuccess, peer) = self.getPeer()
        if !isSuccess {
            printText(text: "receiveMsg 未获取到peer")
            return
        }
        self.watchConnect?.registerReceiver(peer!, { [self] message in
            if message.type == 0 {
                if let str = String(bytes: message.data, encoding:.utf8) {
                    self.printText(text: str)
                }
            } else if message.type == 1 {
                if let str = String(bytes: message.data, encoding:.utf8) {
                    self.printText(text: str)
                }
            } else {
                self.printText(text: "message type error!!")
            }
        })
    }
    @objc
    func cancelReceiveMsg() {
        printText(text: "cancelReceiveMsg")
        let (isSuccess, peer) = self.getPeer()
        if !isSuccess {
            printText(text: "cancelReceiveMsg 未获取到peer")
            return
        }
        self.watchConnect?.unregisterReceiver(peer!, { [self] message in
            if message.type == 0 {
                if let str = String(bytes: message.data, encoding:.utf8) {
                    self.printText(text: str)
                }
            } else if message.type == 1 {
                if let str = String(bytes: message.data, encoding:.utf8) {
                    self.printText(text: str)
                }
            } else {
                self.printText(text: "message type error!!")
            }
        })
    }
    @objc
    func notification() {
        printText(text: "notification")
    }
    @objc
    func clearPrint() {
        printText(text: "")
    }
    func printText(text: String) {
        DispatchQueue.main.async {
            self.printLabel?.text = text
        }
    }
    func getPeer() -> (isGetSuccess: Bool, peer: Peer?) {
        guard let sDvice = selectedDevice else {
            printText(text: "未选中设备")
            return (false, nil)
        }
        guard let appPkgNameText = appPkgNameTextField?.text else {
            printText(text: "未输入appPkgName")
            return (false, nil)
        }
        guard let bundleName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String else {
            printText(text: "获取当前应用包名失败")
            return (false, nil)
        }
        let device = Device(model: sDvice.model, name: sDvice.name, uuid: sDvice.uuid, productType: sDvice.productType, reservedness: sDvice.reservedness, softwareVersion: sDvice.softwareVersion, isConnected: sDvice.isConnected, p2pCapability: sDvice.p2pCapability, notifyCapability: sDvice.notifyCapability, sensorCapability: sDvice.sensorCapability, deviceType: sDvice.deviceType)
        let peer = Peer(device: device, fingerPrint: "", srcPkgName: bundleName, dstPkgName: appPkgNameText)
        return (true, peer)
    }
    var tableView: UITableView? = nil
    var deviceList: [Device] = []
    var selectedDevice: Device? = nil
    var appPkgNameTextField: UITextField? = nil
    var msgContentTextField: UITextField? = nil
    var dstFilePathTextField: UITextField? = nil
    var getBoundDevicesButton: UIButton? = nil
    var getAvailableKbytesButton: UIButton? = nil
    var p2pDesLabel: UILabel? = nil
    var pingAppButton: UIButton? = nil
    var isAppInstalledButton: UIButton? = nil
    var getAppVersionButton: UIButton? = nil
    var sendMsgButton: UIButton? = nil
    var sendFileButton: UIButton? = nil
    var cancelSendFileButton: UIButton? = nil
    var receiveMsgButton: UIButton? = nil
    var cancelReceiveMsgButton: UIButton? = nil
    var registerOrUnregisterDesLabel: UILabel? = nil
    var notificationButton: UIButton? = nil
    var clearPrintButton: UIButton? = nil
    var printLabel: UILabel? = nil
    var watchConnect: WatchConnectEngine? = nil
    let screenWidth: CGFloat = UIScreen.main.bounds.width
    
    
    private func initTitle() {
        let title = UILabel()
        title.text = "Bound Device List"
        title.textColor = .lightGray
        title.font = .systemFont(ofSize: 16)
        title.frame = CGRect(x: 0, y: 48, width: screenWidth, height: 40)
        title.textAlignment = .center
        view.addSubview(title)
    }
    private func initTableView() {
        if tableView == nil{
            tableView = UITableView()
            tableView!.frame = CGRect(x: 10, y: 92, width: screenWidth - 20, height: 200)
            tableView!.dataSource = self
            tableView!.delegate = self
            tableView?.layer.borderWidth = 1
            tableView?.layer.borderColor = UIColor.lightGray.cgColor
            tableView?.separatorStyle = .none
            view.addSubview(tableView!)
        }
    }
    private func initAppPkgNameTextField() {
        if appPkgNameTextField == nil{
            let appPkgNameLabel = UILabel()
            appPkgNameLabel.text = "AppPkgName:"
            appPkgNameLabel.textColor = .lightGray
            appPkgNameLabel.font = .systemFont(ofSize: 14)
            appPkgNameLabel.frame = CGRect(x: 10, y: Int(tableView!.frame.maxY + 4), width: 100, height: 30)
            view.addSubview(appPkgNameLabel)
            
            appPkgNameTextField = UITextField()
            appPkgNameTextField!.frame = CGRect(x: Int(appPkgNameLabel.frame.maxX) + 10, y: Int(appPkgNameLabel.frame.minY), width: Int(screenWidth - appPkgNameLabel.frame.maxX) - 20, height: 30)
            appPkgNameTextField!.placeholder = "appPkgName"
            appPkgNameTextField!.text = "com.example.watchconnect"
            appPkgNameTextField!.returnKeyType = .done
            appPkgNameTextField!.delegate = self
            appPkgNameTextField!.layer.borderWidth = 1
            appPkgNameTextField!.layer.borderColor = UIColor.lightGray.cgColor
            view.addSubview(appPkgNameTextField!)
        }
    }
    private func initMsgContentTextField() {
        if msgContentTextField == nil{
            let msgContentLabel = UILabel()
            msgContentLabel.text = "msgContent:"
            msgContentLabel.textColor = .lightGray
            msgContentLabel.font = .systemFont(ofSize: 14)
            msgContentLabel.frame = CGRect(x: 10, y: Int(appPkgNameTextField!.frame.maxY + 4), width: 100, height: 30)
            view.addSubview(msgContentLabel)
            
            msgContentTextField = UITextField()
            msgContentTextField!.frame = CGRect(x: Int(msgContentLabel.frame.maxX) + 10, y: Int(msgContentLabel.frame.minY), width: Int(screenWidth - msgContentLabel.frame.maxX) - 20, height: 30)
            msgContentTextField!.placeholder = "msgContent"
            msgContentTextField!.returnKeyType = .done
            msgContentTextField!.delegate = self
            msgContentTextField!.layer.borderWidth = 1
            msgContentTextField!.layer.borderColor = UIColor.lightGray.cgColor
            view.addSubview(msgContentTextField!)
        }
    }
    private func initDstFilePathTextField() {
        if dstFilePathTextField == nil{
            let dstFilePathNameLabel = UILabel()
            dstFilePathNameLabel.text = "dstFilePath:"
            dstFilePathNameLabel.textColor = .lightGray
            dstFilePathNameLabel.font = .systemFont(ofSize: 14)
            dstFilePathNameLabel.frame = CGRect(x: 10, y: Int(msgContentTextField!.frame.maxY + 4), width: 100, height: 30)
            view.addSubview(dstFilePathNameLabel)
            
            dstFilePathTextField = UITextField()
            dstFilePathTextField!.frame = CGRect(x: Int(dstFilePathNameLabel.frame.maxX) + 10, y: Int(dstFilePathNameLabel.frame.minY), width: Int(screenWidth - dstFilePathNameLabel.frame.maxX) - 20, height: 30)
            dstFilePathTextField!.placeholder = "dstFilePath"
            dstFilePathTextField!.returnKeyType = .done
            dstFilePathTextField!.delegate = self
            dstFilePathTextField!.layer.borderWidth = 1
            dstFilePathTextField!.layer.borderColor = UIColor.lightGray.cgColor
            view.addSubview(dstFilePathTextField!)
        }
    }
    private func initGetBoundDevicesButton() {
        if getBoundDevicesButton == nil{
            let getBoundDevicesButtonLabel = UILabel()
            getBoundDevicesButtonLabel.text = "Obtains bound device:"
            getBoundDevicesButtonLabel.textColor = .lightGray
            getBoundDevicesButtonLabel.font = .systemFont(ofSize: 14)
            getBoundDevicesButtonLabel.frame = CGRect(x: 10, y: Int(dstFilePathTextField!.frame.maxY + 4), width: 150, height: 30)
            view.addSubview(getBoundDevicesButtonLabel)
            
            getBoundDevicesButton = UIButton(type: .system)
            getBoundDevicesButton!.frame = CGRect(x: Int(getBoundDevicesButtonLabel.frame.maxX) + 10, y: Int(dstFilePathTextField!.frame.maxY + 10), width: Int(screenWidth - getBoundDevicesButtonLabel.frame.maxX) - 20, height: 30)
            getBoundDevicesButton!.setTitle("getBoundDevices", for: .normal)
            getBoundDevicesButton!.setTitleColor(.white, for: .normal)
            getBoundDevicesButton!.addTarget(self, action: #selector(getBoundDevices), for: .touchUpInside)
            getBoundDevicesButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            getBoundDevicesButton!.layer.cornerRadius = 2
            view.addSubview(getBoundDevicesButton!)
        }
    }
    private func initGetAvailableKbytesButton() {
        if getAvailableKbytesButton == nil{
            let getAvailableKbytesButtonLabel = UILabel()
            getAvailableKbytesButtonLabel.text = "Device's Available Kbytes:"
            getAvailableKbytesButtonLabel.textColor = .lightGray
            getAvailableKbytesButtonLabel.font = .systemFont(ofSize: 14)
            getAvailableKbytesButtonLabel.frame = CGRect(x: 10, y: Int(getBoundDevicesButton!.frame.maxY + 4), width: 180, height: 30)
            view.addSubview(getAvailableKbytesButtonLabel)
            
            getAvailableKbytesButton = UIButton(type: .system)
            getAvailableKbytesButton!.frame = CGRect(x: Int(getAvailableKbytesButtonLabel.frame.maxX) + 10, y: Int(getBoundDevicesButton!.frame.maxY + 10), width: Int(screenWidth - getAvailableKbytesButtonLabel.frame.maxX) - 20, height: 30)
            getAvailableKbytesButton!.setTitle("getAvailableKbytes", for: .normal)
            getAvailableKbytesButton!.setTitleColor(.white, for: .normal)
            getAvailableKbytesButton!.addTarget(self, action: #selector(getAvailableKbytes), for: .touchUpInside)
            getAvailableKbytesButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            getAvailableKbytesButton!.layer.cornerRadius = 2
            view.addSubview(getAvailableKbytesButton!)
        }
    }
    
    private func initP2pDesLabel() {
        if p2pDesLabel == nil{
            p2pDesLabel = UILabel()
            p2pDesLabel!.text = "P2P communication and Msg managerment:"
            p2pDesLabel!.textColor = .lightGray
            p2pDesLabel!.font = .systemFont(ofSize: 14)
            p2pDesLabel!.frame = CGRect(x: 10, y: Int(getAvailableKbytesButton!.frame.maxY + 4), width: Int(screenWidth) - 20, height: 30)
            p2pDesLabel!.textAlignment = .left
            view.addSubview(p2pDesLabel!)
        }
    }
    private func initPingAppButton() {
        if pingAppButton == nil{
            pingAppButton = UIButton(type: .system)
            pingAppButton!.frame = CGRect(x: 10, y: Int(p2pDesLabel!.frame.maxY + 4), width: 80, height: 30)
            pingAppButton!.setTitle("pingApp", for: .normal)
            pingAppButton!.setTitleColor(.white, for: .normal)
            pingAppButton!.addTarget(self, action: #selector(pingApp), for: .touchUpInside)
            pingAppButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            pingAppButton!.layer.cornerRadius = 2
            view.addSubview(pingAppButton!)
        }
    }
    private func initIsAppInstalledButton() {
        if isAppInstalledButton == nil{
            isAppInstalledButton = UIButton(type: .system)
            isAppInstalledButton!.frame = CGRect(x: Int(pingAppButton!.frame.maxX + 10), y: Int(p2pDesLabel!.frame.maxY + 4), width: 120, height: 30)
            isAppInstalledButton!.setTitle("isAppInstalled", for: .normal)
            isAppInstalledButton!.setTitleColor(.white, for: .normal)
            isAppInstalledButton!.addTarget(self, action: #selector(isAppInstalled), for: .touchUpInside)
            isAppInstalledButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            isAppInstalledButton!.layer.cornerRadius = 2
            view.addSubview(isAppInstalledButton!)
        }
    }
    private func initGetAppVersionButton() {
        if getAppVersionButton == nil{
            getAppVersionButton = UIButton(type: .system)
            getAppVersionButton!.frame = CGRect(x: Int(isAppInstalledButton!.frame.maxX + 10), y: Int(p2pDesLabel!.frame.maxY + 4), width: Int(screenWidth - isAppInstalledButton!.frame.maxX - 20), height: 30)
            getAppVersionButton!.setTitle("getAppVersion", for: .normal)
            getAppVersionButton!.setTitleColor(.white, for: .normal)
            getAppVersionButton!.addTarget(self, action: #selector(getAppVersion), for: .touchUpInside)
            getAppVersionButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            getAppVersionButton!.layer.cornerRadius = 2
            view.addSubview(getAppVersionButton!)
        }
    }
    private func initSendMsgButton() {
        if sendMsgButton == nil{
            sendMsgButton = UIButton(type: .system)
            sendMsgButton!.frame = CGRect(x: 10, y: Int(pingAppButton!.frame.maxY + 4), width: 85, height: 30)
            sendMsgButton!.setTitle("sendMsg", for: .normal)
            sendMsgButton!.setTitleColor(.white, for: .normal)
            sendMsgButton!.addTarget(self, action: #selector(sendMsg), for: .touchUpInside)
            sendMsgButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            sendMsgButton!.layer.cornerRadius = 2
            view.addSubview(sendMsgButton!)
        }
    }
    private func initSendFileButton() {
        if sendFileButton == nil{
            sendFileButton = UIButton(type: .system)
            sendFileButton!.frame = CGRect(x: Int(sendMsgButton!.frame.maxX + 10), y: Int(pingAppButton!.frame.maxY + 4), width: 85, height: 30)
            sendFileButton!.setTitle("sendFile", for: .normal)
            sendFileButton!.setTitleColor(.white, for: .normal)
            sendFileButton!.addTarget(self, action: #selector(sendFile), for: .touchUpInside)
            sendFileButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            sendFileButton!.layer.cornerRadius = 2
            view.addSubview(sendFileButton!)
        }
    }
    private func initCancelSendFileButton() {
        if cancelSendFileButton == nil{
            cancelSendFileButton = UIButton(type: .system)
            cancelSendFileButton!.frame = CGRect(x: Int(sendFileButton!.frame.maxX + 10), y: Int(pingAppButton!.frame.maxY + 4), width: Int(screenWidth - sendFileButton!.frame.maxX - 20) , height: 30)
            cancelSendFileButton!.setTitle("cancelSendFile", for: .normal)
            cancelSendFileButton!.setTitleColor(.white, for: .normal)
            cancelSendFileButton!.addTarget(self, action: #selector(cancelSendFile), for: .touchUpInside)
            cancelSendFileButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            cancelSendFileButton!.layer.cornerRadius = 2
            view.addSubview(cancelSendFileButton!)
        }
    }
    private func initReceiveMsgButton() {
        if receiveMsgButton == nil{
            receiveMsgButton = UIButton(type: .system)
            receiveMsgButton!.frame = CGRect(x: 10, y: Int(sendMsgButton!.frame.maxY + 4), width: Int(screenWidth - 30) / 2, height: 30)
            receiveMsgButton!.setTitle("receiveMsg", for: .normal)
            receiveMsgButton!.setTitleColor(.white, for: .normal)
            receiveMsgButton!.addTarget(self, action: #selector(receiveMsg), for: .touchUpInside)
            receiveMsgButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            receiveMsgButton!.layer.cornerRadius = 2
            view.addSubview(receiveMsgButton!)
        }
    }
    private func initCancelReceiveMsgButton() {
        if cancelReceiveMsgButton == nil{
            cancelReceiveMsgButton = UIButton(type: .system)
            cancelReceiveMsgButton!.frame = CGRect(x: Int(receiveMsgButton!.frame.maxX + 10), y: Int(sendMsgButton!.frame.maxY + 4), width: Int(screenWidth - 30) / 2, height: 30)
            cancelReceiveMsgButton!.setTitle("cancelReceiveMsg", for: .normal)
            cancelReceiveMsgButton!.setTitleColor(.white, for: .normal)
            cancelReceiveMsgButton!.addTarget(self, action: #selector(cancelReceiveMsg), for: .touchUpInside)
            cancelReceiveMsgButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            cancelReceiveMsgButton!.layer.cornerRadius = 2
            view.addSubview(cancelReceiveMsgButton!)
        }
    }
    private func initRegisterOrUnregisterDesLabel() {
        if registerOrUnregisterDesLabel == nil{
            registerOrUnregisterDesLabel = UILabel()
            registerOrUnregisterDesLabel!.text = "Register or unregister a listener for monitoring the connection status: "
            registerOrUnregisterDesLabel!.textColor = .lightGray
            registerOrUnregisterDesLabel!.font = .systemFont(ofSize: 12)
            registerOrUnregisterDesLabel!.frame = CGRect(x: 10, y: Int(receiveMsgButton!.frame.maxY + 4), width: Int(screenWidth - 20), height: 30)
            registerOrUnregisterDesLabel!.textAlignment = .left
            registerOrUnregisterDesLabel!.numberOfLines = 0
            registerOrUnregisterDesLabel!.sizeToFit()
            view.addSubview(registerOrUnregisterDesLabel!)
        }
    }
    private func initNotificationButton() {
        if notificationButton == nil{
            notificationButton = UIButton(type: .system)
            notificationButton!.frame = CGRect(x: 10, y: Int(registerOrUnregisterDesLabel!.frame.maxY + 4), width: Int(screenWidth - 30)/2 + 10, height: 30)
            notificationButton!.setTitle("Notification", for: .normal)
            notificationButton!.setTitleColor(.white, for: .normal)
            notificationButton!.addTarget(self, action: #selector(notification), for: .touchUpInside)
            notificationButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            notificationButton!.layer.cornerRadius = 2
            view.addSubview(notificationButton!)
        }
    }
    private func initClearPrintButton() {
        if clearPrintButton == nil{
            clearPrintButton = UIButton(type: .system)
            clearPrintButton!.frame = CGRect(x: Int(notificationButton!.frame.maxX + 10), y: Int(registerOrUnregisterDesLabel!.frame.maxY + 4), width: Int(screenWidth-30) / 2 - 10, height: 30)
            clearPrintButton!.setTitle("clearPrint", for: .normal)
            clearPrintButton!.setTitleColor(.white, for: .normal)
            clearPrintButton!.addTarget(self, action: #selector(clearPrint), for: .touchUpInside)
            clearPrintButton!.backgroundColor = UIColor(red: 89 / 255, green: 13 / 255, blue: 225 / 255, alpha: 1)
            clearPrintButton!.layer.cornerRadius = 2
            view.addSubview(clearPrintButton!)
        }
    }
    private func initPrintLabel() {
        if printLabel == nil {
            printLabel = UILabel()
            printLabel!.textColor = .black
            printLabel!.font = .systemFont(ofSize: 16)
            printLabel!.frame = CGRect(x: 10, y: Int(notificationButton!.frame.maxY + 4), width: Int(screenWidth - 20), height: 50)
            printLabel!.textAlignment = .left
            printLabel!.numberOfLines = 0
            printLabel!.lineBreakMode = .byWordWrapping
            view.addSubview(printLabel!)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.watchConnect = WatchConnectEngine()
        self.initTitle()
        self.initTableView()
        self.initAppPkgNameTextField()
        self.initMsgContentTextField()
        self.initDstFilePathTextField()
        self.initGetBoundDevicesButton()
        self.initGetAvailableKbytesButton()
        self.initP2pDesLabel()
        self.initPingAppButton()
        self.initIsAppInstalledButton()
        self.initGetAppVersionButton()
        self.initSendMsgButton()
        self.initSendFileButton()
        self.initCancelSendFileButton()
        self.initReceiveMsgButton()
        self.initCancelReceiveMsgButton()
        self.initRegisterOrUnregisterDesLabel()
        self.initNotificationButton()
        self.initClearPrintButton()
        self.initPrintLabel()
    }
}

