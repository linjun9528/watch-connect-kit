# watch-connect-kit android demo开发流程

demo主要是介绍手机上三方应用如何集成和使用社区提供的 `watch-connect-kit` ，连接 `watch-connect-app` 与 `watch` 上的应用通讯。

## 1.demo开发环境

Android Studio: 2024.1.1 Patch 1。

Android SDK: 35。

Android Gradle Plugin Version: 8.5.0。

Gradle Version: 8.7。

Java Version: 1.8

## 2.集成kit准备

### 2.1.kit下载

`watch-connect-kit` 获取链接：[点我下载](kit/android/)。

### 2.2.工程集成kit

2.2.1.在工程 `app` 目录下新建 `libs` 文件夹，将下载的 `watch-connect-kit.aar` 拷贝到 `libs` 中。

2.2.2.在 `build.gradle` 中添加依赖。

```gradle
dependencies {
    ......
    implementation files('libs/watch-connect-kit.aar')
    ......
}
```

### 2.3.kit接口定义

社区 `watch-connect-kit` 提供了接口定义及使用到的结构体等信息，可先查看熟悉。

kit接口定义链接：[点我查看](https://gitee.com/cooperation-team-L0UI/doc/tree/master/%E6%89%8B%E6%9C%BA%E6%89%8B%E8%A1%A8%E4%BA%92%E8%81%94%E5%BC%80%E5%8F%91/phone)。

### 2.4.Demo

demo链接：[点我查看](demo/watch)。

## 3.集成kit开发

### 3.1.权限配置

`watch-connect-kit-demo` 会使用到文件传输功能，需要添加存储权限相关功能。需要注意的是，在不同版本中，存储权限配置和申请方式不同，需要单独处理。

### 3.2.kit初始化

3.2.1.自定义类继承 `Application` ，在 `onCreate` 中初始化 `watch-connect-kit` ，在手机上可能会有多个 `watch-connect-app` 提供服务，可以将这些服务都查询保存起来待后续调用时选择。所有服务的 **ACTION** 都指定为 **OH_WATCH_CONNECT_ACTION** 。

参考代码：

```java
public class App extends Application {
    private static final String TAG = "test-kitDemo";
    private static final Map<String, WatchConnectEngine> kits = new HashMap<>();
    @Override
    public void onCreate() {
        super.onCreate();

        initWatchConnectKit();
    }

    private void initWatchConnectKit() {
        PackageManager packageManager = getPackageManager();
        Intent intent = new Intent();
        intent.setAction("OH_WATCH_CONNECT_ACTION");
        List<ResolveInfo> services = packageManager.queryIntentServices(intent, 0);
        for (ResolveInfo server : services) {
            Log.i(TAG, "MainActivity test serviceInfo.package name:" + server.serviceInfo.packageName);
            WatchConnectEngine engine = new WatchConnectEngine();
            boolean ret = engine.init("appId-1001-2001", "authId-6001-8001", getApplicationContext(), server.serviceInfo.packageName);
            if (ret) {
                Log.i("test-kitDemo", "App -> WatchConnectEngine init packageName(" + server.serviceInfo.packageName + ") success.");
            } else {
                Log.e("test-kitDemo", "App -> WatchConnectEngine init packageName(" + server.serviceInfo.packageName + ") fail.");
            }

            kits.put(server.serviceInfo.packageName, engine);
        }
    }

    public static WatchConnectEngine getWatchConnectEngine(String packageName) {
        return kits.getOrDefault(packageName, null);
    }
}
```

3.2.2.将 `AndroidManifest.xml` 文件中 `application` 配置的默认 `name` 修改为自定义Application名称 `App`。

```xml
<application
    android:name=".App"
    ......
>
    ......
</application>
```

### 3.3.调用kit接口方法

3.3.1.导入包，其中入口是 `WatchConnectEngine` ,其它的都由 `WatchConnectEngine` 引出。这一步可由AndroidStudio工具自动完成。

```java
import com.openharmony.watch.aidl.CancelFileTransferCallBack;
import com.openharmony.watch.aidl.Device;
import com.openharmony.watch.aidl.DeviceCallback;
import com.openharmony.watch.aidl.Message;
import com.openharmony.watch.aidl.Notification;
import com.openharmony.watch.aidl.NotifyCallback;
import com.openharmony.watch.aidl.Peer;
import com.openharmony.watch.aidl.PingCallback;
import com.openharmony.watch.aidl.Receiver;
import com.openharmony.watch.aidl.SendCallback;
import com.openharmony.watch.engine.WatchConnectEngine;
```

3.3.2.在自定义的 `App` 类中，已经获取所有服务都保存在 `Map<String, WatchConnectEngine>` ，在大多数情况下手机只安装了一个服务端，可以直接获取到对应的 `WatchConnectEngine` 实例;少数情况会安装多个服务端，此时可以弹框让用户选择其中一个服务。

3.3.3.选择服务的 `Activity` 示例布局：

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:id="@+id/main"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:layout_centerHorizontal="true"
    android:layout_margin="8dp"
    android:orientation="vertical"
    tools:context="com.openharmony.watch.SelectServiceActivity">

</LinearLayout>
```

3.3.4.选择服务的 `Activity` ，获取包名。

示例代码：

```java
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    EdgeToEdge.enable(this);
    setContentView(R.layout.activity_select_service);
    ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
        Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
        v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
        return insets;
    });

    layout = findViewById(R.id.main);
    PackageManager packageManager = getPackageManager();
    Intent intent = new Intent();
    intent.setAction("watchConnectApp.service.GET_KIT_SERVICE_ACTION");
    List<ResolveInfo> services = packageManager.queryIntentServices(intent, 0);
    for (ResolveInfo server : services) {
        Button btn = new Button(this);
        btn.setText(server.serviceInfo.packageName);
        btn.setOnClickListener(v -> {
            Intent it = new Intent();
            it.putExtra("packageName", server.serviceInfo.packageName);
            setResult(RESULT_OK, it);
            finish();
        });

        layout.addView(btn);
    }
}
```

3.3.5.通过 `Activity` 返回的结果拿到包名，选择对应的 `WatchConnectEngine` 实例。

示例代码：

```java
ActivityResultLauncher<Intent> serviceLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
    Log.i(TAG, "serviceLauncher -> onActivityResult");
    if (result != null && result.getData() != null) {
        String packageName = result.getData().getStringExtra("packageName");
        // 通过包名从 app 的 kits 中获取指定 engine
        engine = App.getWatchConnectEngine(packageName);
        // 处理后续业务
        ......
    } else {
        Toast.makeText(this, "Must select a service!!", Toast.LENGTH_SHORT).show();
    }
});
......

Intent intent = new Intent("com.openharmony.watch.SELECT_SERVICE_ACTIVITY");
serviceLauncher.launch(intent);
```

3.3.6.以获取配对设备接口为例，通过 `WatchConnectEngine` 的实例调用 `getBondedDevices` 之后，会通过传入 `DeviceCallback` 的 `onGetResult` 回调回结果，拿到 `devices` 后即可做后续业务。

```java
engine.getBondedDevices(new DeviceCallback.Stub() {
    @Override
    public void onGetResult(List<Device> devices) throws RemoteException {
        Log.i(TAG, "getBondedDevices result, devices:" + devices);

        ......
    }
});
```

3.3.7.其它接口可以参考 `getBondedDevices` 的调用方式。

## 4.注意事项

### 4.1.权限问题

开发会需要存储，网络等权限，在 `AndroidManifest.xml` 中申明，在需要权限的操作前，最好动态检查和申请，建议使用 `EasyPermissions`，`RxPermissions` 等权限管理工具。

### 4.2.线程问题

执行长时任务时，不要在UI线程执行，建议使用RxAndroid等库来调度线程的切换。

### 4.3.获取kit服务问题

1.如果手机上只安装了一个集成kit服务端时，其中 `3.3.3 - 3.3.5` 等几步可不用执行。

2.调用 `WatchConnectEngine` 的 `init` 时，内部会通过 `bindService` 来获取与服务端的连接，由于android机制原因， `ServiceConnection` 的 `onServiceConnected` 在调用 `bindService` 后不是同步连接的，如果在 `init` 执行后立即执行 `WatchConnectEngine` 其它函数，会出现还未连接到服务的情况。建议 `init` 尽量提前执行。
