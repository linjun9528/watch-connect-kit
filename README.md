# Watch Connect kit

## 介绍

Connect Kit主要是提供给手机三方应用与Watch三方应用进行通讯，分为两个，一个是穿戴侧, 一个是手机侧;   主要功能包括：设备管理，P2P消息发送(message/file),  notify消息通知功能

## 架构定义

架构设计：[点我查看](https://gitee.com/cooperation-team-L0UI/doc/blob/master/手机手表互联开发/readme.md)

## 穿戴侧

### 1. API定义 

Watch Connect kit(穿戴侧)：[点我查看](https://gitee.com/cooperation-team-L0UI/doc/tree/master/手机手表互联开发/watch)

### 2. Demo

demo bin：[点我下载](test-tool-packages)

demo源代码：[点我查看](demo/watch)

![](assets/watch-connect-kit-watch.png)

## 手机侧

目前已经在 **android**，**ios**，**openharmony** 3个平台提供了对应的接口定义，demo和集成开发指导，开发者可根据需要获取。

### 1. API定义

Watch Connect kit(android)：[点我查看](https://gitee.com/cooperation-team-L0UI/doc/tree/master/手机手表互联开发/phone/watch-connect-kit-android.md)

Watch Connect kit(ios)：[点我查看](https://gitee.com/cooperation-team-L0UI/doc/blob/master/手机手表互联开发/phone/watch-connect-kit-iOS.md)

Watch Connect kit(openharmony)：[点我查看](https://gitee.com/cooperation-team-L0UI/doc/blob/master/手机手表互联开发/phone/watch-connect-kit-openHarmony.md)

### 2. Watch Connect kit集成

android集成指导：[点我查看](watch-connect-kit-demo-doc-android.md)

ios集成指导：[点我查看](watch-connect-kit-demo-doc-ios.md)

openharmony集成指导：[点我查看](watch-connect-kit-demo-doc-openharmony.md)

### 3. Demo

**andriod：**

demo：[点我下载](test-tool-packages)

demo源代码：[点我查看](demo/phone/android)

![](assets/watch-connect-kit-demo.png)

**ios：**

demo：[点我下载](test-tool-packages)

demo源代码：[点我查看](demo/phone/iOS)

**openharmony：**

demo：[点我下载](test-tool-packages)

demo源代码：[点我查看](demo/phone/openharmony)
