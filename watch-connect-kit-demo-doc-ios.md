# watch-connect-kit iOS demo开发流程

demo主要是介绍手机上三方应用如何集成和使用社区提供的 `watch-connect-kit` ，与 `watch` 上的应用通讯。

## 1.demo开发环境

Xcode 16.1

iOS SDK: 17.5

iOS系统：iOS 15.6-iOS 18.0

## 2.集成kit准备

### 2.1.kit下载

`watch-connect-kit` 获取链接：[点我下载](https://gitee.com/cooperation-team-L0UI/watch-connect-kit/tree/master/kit/iOS/WatchConnectKit.framework)。

### 2.2.工程集成kit

2.2.1.创建工程
点击xcode的File-New-Project,选择创建iOS-APP工程（命名：WatchAppDemo），打开工程拖动WatchConnectKit.framework到WatchAppDemo文件夹

2.2.2.添加蓝牙权限
点击.xcodeproj在info.plist中添加蓝牙权限 “Privacy - Bluetooth Always Usage Description”，value内容设置为：请求获取蓝牙

### 2.3.kit接口定义

社区 `watch-connect-kit` 提供了接口定义及使用到的结构体等信息，可先查看熟悉。

kit接口定义链接：[点我查看](https://gitee.com/cooperation-team-L0UI/doc/blob/master/%E6%89%8B%E6%9C%BA%E6%89%8B%E8%A1%A8%E4%BA%92%E8%81%94%E5%BC%80%E5%8F%91/phone/watch-connect-kit-iOS.md)。

### 2.4.Demo

demo链接：[点我查看](https://gitee.com/cooperation-team-L0UI/watch-connect-kit/tree/master/demo/phone/iOS)。

## 3.集成kit开发

### 3.1.kit初始化

在工程自动创建的ViewController类中定义属性    var watchConnect: WatchConnectEngine? = nil
并在videDidLoad方法中进行初始化：self.watchConnect = WatchConnectEngine()
创建列表以便于显示蓝牙列表

参考代码：

```swift
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIDocumentPickerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let device = deviceList[indexPath.item]
        let checkBox = UIView()
        checkBox.frame = CGRect(origin: CGPoint(x: 20, y: 10), size: CGSize(width: 20, height: 20))
        checkBox.layer.cornerRadius = 10
        checkBox.layer.borderColor = selectedDevice?.name == device.name ? UIColor.cyan.cgColor : UIColor.lightGray.cgColor
        checkBox.layer.borderWidth = 2
        
        let checkBox1 = UIView()
        
        checkBox1.frame = CGRect(origin: CGPoint(x: 25, y: 15), size: CGSize(width: 10, height: 10))
        checkBox1.layer.cornerRadius = 5
        checkBox1.backgroundColor =  selectedDevice?.name == device.name ? .cyan : UIColor.clear
        cell.addSubview(checkBox)
        cell.addSubview(checkBox1)
        
        let titleLabel = UILabel()
        titleLabel.text = deviceList[indexPath.item].name
        titleLabel.frame = CGRect(x: 50, y: 5, width: 300, height: 30)
        cell.addSubview(titleLabel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedDevice = deviceList[indexPath.item]
        tableView.reloadData()
    }
    var tableView: UITableView? = nil
    var deviceList: [Device] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.watchConnect = WatchConnectEngine()
        if tableView == nil{
            tableView = UITableView()
            tableView!.frame = CGRect(x: 10, y: 98, width: width-20, height: 200)
            tableView!.dataSource = self
            tableView!.delegate = self
            tableView?.layer.borderWidth = 1
            tableView?.layer.borderColor = UIColor.lightGray.cgColor
            tableView?.separatorStyle = .none
            view.addSubview(tableView!)
        }
    }
}
```

### 3.2.调用kit接口方法

3.2.1.导入头文件frameworker

```swift 
import WatchConnectKit
```

3.2.2.创建三个TextField，用于显示当前app的Bundle name、文本消息内容、文件路径

```swift
var appPkgNameTextField: UITextField? = nil
var msgContentTextField: UITextField? = nil
var dstFilePathTextField: UITextField? = nil

// 在viewDidLoad中添加代码

if appPkgNameTextField == nil{
    let appPkgNameLabel = UILabel()
    appPkgNameLabel.text = "AppPkgName:"
    appPkgNameLabel.textColor = .lightGray
    appPkgNameLabel.font = .systemFont(ofSize: 14)
    appPkgNameLabel.frame = CGRect(x: 0, y: Int((tableView?.frame.maxY)! + 10), width: 100, height: 30)
    appPkgNameLabel.textAlignment = .center
    view.addSubview(appPkgNameLabel)
    
    appPkgNameTextField = UITextField()
    appPkgNameTextField?.frame = CGRect(x: Int(appPkgNameLabel.frame.maxX)+20, y: Int(appPkgNameLabel.frame.minY), width: Int(width-appPkgNameLabel.frame.maxX)-70, height: 30)
    appPkgNameTextField?.placeholder = "appPkgName"
    appPkgNameTextField?.text = "com.example.watchconnect"
    appPkgNameTextField?.returnKeyType = .done
    appPkgNameTextField?.delegate = self
    appPkgNameTextField?.layer.borderWidth = 1
    appPkgNameTextField?.layer.borderColor = UIColor.lightGray.cgColor
    view.addSubview(appPkgNameTextField!)
}

if msgContentTextField == nil{
    .......
}
if dstFilePathTextField == nil{
    .......
            
}

func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    appPkgNameTextField?.resignFirstResponder()
    msgContentTextField?.resignFirstResponder()
    dstFilePathTextField?.resignFirstResponder()
    return true
}

```

3.2.3.添加功能按钮，按钮事件对应 `WatchConnectEngine` 相关接口。

示例代码：

```swift
// viewDidLoad中添加按钮
if getBoundDevicesButton == nil{
                
    getBoundDevicesButton = UIButton(type: .system)
    getBoundDevicesButton?.frame = CGRect(x: Int(getBoundDevicesButtonLabel.frame.maxX)+5, y: Int((dstFilePathTextField?.frame.maxY)!+10), width: Int(width-getBoundDevicesButtonLabel.frame.maxX)-10, height: 30)
    getBoundDevicesButton?.setTitle("getBoundDevices", for: .normal)
    getBoundDevicesButton?.setTitleColor(.white, for: .normal)
    getBoundDevicesButton?.addTarget(self, action: #selector(getBoundDevices), for: .touchUpInside)
    getBoundDevicesButton?.backgroundColor = UIColor(red: 89/255, green: 13/255, blue: 225/255, alpha: 1)
    getBoundDevicesButton?.layer.cornerRadius = 2

    view.addSubview(getBoundDevicesButton!)
}
if pingAppButton == nil{
    ......
}
if isAppInstalledButton == nil{
    ......
        
}
if getAppVersionButton == nil{
    .......
        
}
if sendMsgButton == nil{
    ......
    view.addSubview(sendMsgButton!)
}
if sendFileButton == nil{
    .......
    view.addSubview(sendFileButton!)
}
if cancelSendFileButton == nil{
    ......

    view.addSubview(cancelSendFileButton!)
}
if receiveMsgButton == nil{
    ......
    view.addSubview(receiveMsgButton!)
}
if cancelReceiveMsgButton == nil{
    ......
    view.addSubview(cancelReceiveMsgButton!)
}
if getAvailableKbytesButton == nil{
    ......
    view.addSubview(getAvailableKbytesButton!)
}
// 实现按钮事件 
@objc
func getBoundDevices() {
    printText(text: "getBoundDevices")
    self.deviceList.removeAll()
    self.watchConnect?.getBondedDevices(callBack: { watchConnectDeviceList in
        self.deviceList = watchConnectDeviceList
        self.tableView?.reloadData()
    })
}

@objc
func pingApp() {
    printText(text: "pingApp")
    let (isSuccess, peer) = self.getPeer()
    if !isSuccess {
        print("未获取到peer")
        printText(text: "pingApp 未获取到peer")
        return
    }

    self.watchConnect?.ping(peer: peer!, pingCallback: { code in
        self.printText(text: "pingApp pingResult：\(code)")
    })
}
......
```

3.2.4.以获取配对设备接口为例，通过 `WatchConnectEngine` 的实例调用 `getBondedDevices` 之后，会通过传入 `callback ` 的回调结果，拿到 `devices` 后即可执行后续业务。
并通过printText方法将返回结果显示在下方文本区域
```swift
@objc
func getBoundDevices() {
    printText(text: "getBoundDevices")
    self.deviceList.removeAll()
    self.watchConnect?.getBondedDevices(callBack: { watchConnectDeviceList in
        self.deviceList = watchConnectDeviceList
        self.tableView?.reloadData()
    })
}
    
if printLabel == nil {
    printLabel = UILabel()
    printLabel!.textColor = .black
    printLabel!.font = .systemFont(ofSize: 16)
    printLabel!.frame = CGRect(x: 10, y: Int((notificationButton?.frame.maxY)!+10), width: Int(width)-20, height: 50)
    printLabel!.textAlignment = .left
    printLabel!.numberOfLines = 0
    printLabel?.lineBreakMode = .byWordWrapping
    view.addSubview(printLabel!)
}
func printText(text: String) {
    printLabel?.text = text
}
```

3.2.5.其它接口可以参考 `getBondedDevices` 的调用方式。